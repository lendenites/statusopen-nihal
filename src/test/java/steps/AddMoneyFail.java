package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class AddMoneyFail 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]> data;
	//String Row[]={"",""};
	String MonitoringData[]= {"TRANSACTION","ADD MONEY","FAIL","",""};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MjksImZpbHRlciI6WyJhbmQiLFsidGltZS1pbnRlcnZhbCIsWyJmaWVsZC1pZCIsMjI1N10sImN1cnJlbnQiLCJkYXkiXSxbIj0iLFsiZmllbGQtaWQiLDIyNDhdLCJBREQgTU9ORVkiXSxbIj0iLFsiZmllbGQtaWQiLDIyNTNdLCJGQUlMIl1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
	public AddMoneyFail(WebDriver driver, CSVWriter writer, List<String[]> data) throws InterruptedException, IOException
	{
		this.driver=driver;
		this.data=data;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(4));
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		//writer.writeNext(Row);
		writer.writeNext(MonitoringData);
		writer.flush();
	}
	 {
		// TODO Auto-generated constructor stub
	}	
}
