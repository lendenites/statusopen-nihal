package steps;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class AssignedToId 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]>data;


	String MonitoringData[]= {"Table","","","Count"};
	String MonitoringCol[]= {"customuser + task + Account","Status-Open","AssignedToId-3024","",""};
	String MonitoringRow[]= {"",""};
    String rowSpace [] = {null};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTU4LCJqb2lucyI6W3siZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjoxNzIsImNvbmRpdGlvbiI6WyI9IixbImZpZWxkLWlkIiw3NTFdLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDU5OF1dXSwiYWxpYXMiOiJMZW5kZW5hcHAgVGFzayJ9LHsiZmllbGRzIjoiYWxsIiwic291cmNlLXRhYmxlIjo4MiwiY29uZGl0aW9uIjpbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwNl1dLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDI2MjFdXV0sImFsaWFzIjoiTGVuZGVuYXBwIEFjY291bnQifV0sImZpbHRlciI6WyJhbmQiLFsiPSIsWyJqb2luZWQtZmllbGQiLCJMZW5kZW5hcHAgQWNjb3VudCIsWyJmaWVsZC1pZCIsMTYzOV1dLCJPUEVOIl0sWyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw2MDJdXSwzMDI0XV0sImFnZ3JlZ2F0aW9uIjpbWyJjb3VudCJdXX0sImRhdGFiYXNlIjoyfSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	public AssignedToId(WebDriver driver,CSVWriter writer,List<String[]>data) throws IOException, InterruptedException
	{
		this.driver=driver;
		this.data=data;
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringCol[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(MonitoringData);

		writer.writeNext(MonitoringRow);
		writer.writeNext(MonitoringCol);
		data.add(rowSpace);
		writer.flush();

	}
}
