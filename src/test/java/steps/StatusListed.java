package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class StatusListed 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]>data;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTcyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzUxXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw1OThdXV0sImFsaWFzIjoiTGVuZGVuYXBwIFRhc2sifSx7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6ODIsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw2MDZdXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBBY2NvdW50IixbImZpZWxkLWlkIiwyNjIxXV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBBY2NvdW50In1dLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDE2MzldXSwiTElTVEVEIl0sWyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw2MDJdXSwzMDI0XSxbInRpbWUtaW50ZXJ2YWwiLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIEFjY291bnQiLFsiZmllbGQtaWQiLDE2NDRdXSwiY3VycmVudCIsImRheSJdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwidHlwZSI6InF1ZXJ5In0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
	String Data[]= {"Table","Status","","Count"},Row[]={""},Col[]= {"Customuser + Lendenapp Task + Lendenapp Account","Listed","",""};
	public StatusListed(WebDriver driver,CSVWriter writer,List<String[]>data) throws InterruptedException, IOException
	{
		this.data=data;
		this.driver=driver;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(6));
		Thread.sleep(2000);
		driver.get(Url);
		Thread.sleep(2000);
        Col[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(Row);
		//writer.writeNext(Data);
		writer.writeNext(Col);
		writer.flush();
	}
	
}
