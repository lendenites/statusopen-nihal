package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class OTP
{
	CSVWriter writer;
	WebDriver driver;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjMzLCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiZmllbGQtaWQiLDIxOTZdLCJjdXJyZW50IiwiZGF5Il1dLCJhZ2dyZWdhdGlvbiI6W1siY291bnQiXV19LCJ0eXBlIjoicXVlcnkifSwiZGlzcGxheSI6InNjYWxhciIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	List<String[]> data;
	String MonitoringData[]= {"LendenUserOTP","","","",""};
	String Row[]= {""};
	public OTP(WebDriver driver,CSVWriter writer,List<String[]> data) throws IOException, InterruptedException
	{
		this.driver=driver;
		this.data=data;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(5));
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(Row);
		writer.writeNext(MonitoringData);
		writer.flush();
		Thread.sleep(2000);
	}


}
