package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class InvestorSignUp
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]>data;
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjE1OCwiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTcyLCJjb25kaXRpb24iOlsiPSIsWyJmaWVsZC1pZCIsNzUxXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw1OThdXV0sImFsaWFzIjoiTGVuZGVuYXBwIFRhc2sifSx7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6ODIsImNvbmRpdGlvbiI6WyI9IixbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBUYXNrIixbImZpZWxkLWlkIiw2MDZdXSxbImpvaW5lZC1maWVsZCIsIkxlbmRlbmFwcCBBY2NvdW50IixbImZpZWxkLWlkIiwyNjIxXV1dLCJhbGlhcyI6IkxlbmRlbmFwcCBBY2NvdW50In1dLCJmaWx0ZXIiOlsiYW5kIixbIj0iLFsiam9pbmVkLWZpZWxkIiwiTGVuZGVuYXBwIFRhc2siLFsiZmllbGQtaWQiLDYwMl1dLDMwMjRdLFsidGltZS1pbnRlcnZhbCIsWyJmaWVsZC1pZCIsNzcwXSwiY3VycmVudCIsImRheSJdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwidHlwZSI6InF1ZXJ5In0sImRpc3BsYXkiOiJ0YWJsZSIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	String MonitoringData[]= {"Lendenapp Customuser + Lendenapp Task + Lendenapp Account","Investor try for sign up","","",""};
	//String Row[]= {"",""};
	public InvestorSignUp(WebDriver driver,CSVWriter writer,List<String[]> data) throws IOException, InterruptedException
	{
		this.driver=driver;
		this.data=data;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(7));
		driver.get(Url);
		Thread.sleep(2000);
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		//writer.writeNext(Row);
		writer.writeNext(MonitoringData);
		writer.flush();
		Thread.sleep(2000);
		
	}

}
