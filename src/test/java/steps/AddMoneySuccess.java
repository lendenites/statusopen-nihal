package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class AddMoneySuccess 
{
	WebDriver driver;
	CSVWriter writer;
	List<String[]>data;
	String Row[]={"",""};
	String MonitoringData[]= {"TRANSACTION","ADD MONEY","SUCCESS","",""};
	String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7ImRhdGFiYXNlIjoyLCJxdWVyeSI6eyJzb3VyY2UtdGFibGUiOjI5LCJmaWx0ZXIiOlsiYW5kIixbInRpbWUtaW50ZXJ2YWwiLFsiZmllbGQtaWQiLDIyNTddLCJjdXJyZW50IiwiZGF5Il0sWyI9IixbImZpZWxkLWlkIiwyMjQ4XSwiQUREIE1PTkVZIl0sWyI9IixbImZpZWxkLWlkIiwyMjUzXSwiU1VDQ0VTUyJdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwidHlwZSI6InF1ZXJ5In0sImRpc3BsYXkiOiJzY2FsYXIiLCJ2aXN1YWxpemF0aW9uX3NldHRpbmdzIjp7fX0=";
	public AddMoneySuccess(WebDriver driver,CSVWriter writer,List<String[]>Data) throws InterruptedException, IOException
	{
		this.driver=driver;
		this.data=data;
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(3));
		driver.get(Url);
		Thread.sleep(10000);
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(Row);
		writer.writeNext(MonitoringData);
		writer.flush();
	}	
}
