package steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.opencsv.CSVWriter;

public class OutstandingRepayment 
{
   WebDriver driver;
   CSVWriter writer;
   List<String[]>data;
   String Url="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MTA1Mywiam9pbnMiOlt7ImZpZWxkcyI6ImFsbCIsInNvdXJjZS10YWJsZSI6MTAwOSwiY29uZGl0aW9uIjpbIj0iLFsiZmllbGQtaWQiLDExNzk1XSxbImpvaW5lZC1maWVsZCIsIkludmVzdG9yIEludmVzdG1lbnRzdW1tYXJ5IixbImZpZWxkLWlkIiwxMjE4OF1dXSwiYWxpYXMiOiJJbnZlc3RvciBJbnZlc3RtZW50c3VtbWFyeSJ9XSwiZmlsdGVyIjpbImFuZCIsWyI9IixbImZpZWxkLWlkIiwxMTc4NV0sMV0sWyI-IixbImpvaW5lZC1maWVsZCIsIkludmVzdG9yIEludmVzdG1lbnRzdW1tYXJ5IixbImZpZWxkLWlkIiwxMjE4M11dLDFdXSwiYWdncmVnYXRpb24iOltbImNvdW50Il1dfSwiZGF0YWJhc2UiOjl9LCJkaXNwbGF5Ijoic2NhbGFyIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319";
   String MonitoringData[]= {"Investor Investment + Investor Investment summary","Tenure is equal to 1","OutstandingRepayment > 1","",""};
   public OutstandingRepayment(WebDriver driver,CSVWriter writer,List<String[]>data) throws IOException
   {
	   this.driver=driver;
	   this.data=data;
	   ((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(2));
		driver.get(Url);
		MonitoringData[3]=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/span/h1")).getText();
		writer.writeNext(MonitoringData);
		writer.flush();
   }
}
