package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.opencsv.CSVWriter;

import steps.AddMoneyFail;
import steps.AddMoneySuccess;
import steps.AssignedToId;
import steps.InvestorSignUp;
import steps.LogIn;
import steps.OTP;
import steps.OutstandingRepayment;
import steps.PaidRepayment;
import steps.StatusListed;
import webCapability.ExcelFile;
import webCapability.WebCapability;

public class MainClass extends WebCapability
{
	public List<String[]> data=new ArrayList<String[]>();
	CSVWriter writer;
	ExcelFile excelFile=new ExcelFile();
	WebDriver driver;
	@BeforeTest
	public void openchrome() throws Exception
	{
		this.driver = WebCapability();
		this.writer=excelFile.ExcelSheet();
		driver.manage().window().maximize();
	}
	@Test(priority = 1)
	public void Login() throws InterruptedException
	{
		new LogIn(driver);
	}
	@Test(priority = 2)
	public void AssignedId() throws IOException, InterruptedException
	{
		new AssignedToId(driver, writer, data);
	}
	@Test(priority = 3)
	public void Paid() throws InterruptedException, IOException
	{
		new PaidRepayment(driver, writer, data);
	}
	@Test(priority = 4)
	public void outstanding() throws IOException
	{
		new OutstandingRepayment(driver, writer, data);
	}
	@Test(priority = 5)
	public void rfsuccess() throws InterruptedException, IOException
	{
		new AddMoneySuccess(driver, writer, data);
	}
	@Test(priority = 6)
	public void Fail() throws InterruptedException, IOException
	{
		new AddMoneyFail(driver, writer, data);
	}
	@Test(priority = 7)
	public void Otp() throws IOException, InterruptedException
	{
		new OTP(driver,writer,data);
	}
	@Test(priority = 8)
	public void listed() throws InterruptedException, IOException
	{
		new StatusListed(driver, writer, data);
	}
	@Test(priority = 9)
	public void signup() throws IOException, InterruptedException
	{
		new InvestorSignUp(driver, writer, data);
	}
	@AfterTest
	public void CloseBrowser()
	{
		driver.quit	();
	}
}
